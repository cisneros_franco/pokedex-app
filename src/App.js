import React, { Component, Fragment } from 'react'



import Header from './layout/Header'
import Main from './layout/Main'
import Footer from './layout/Footer'
const logo = './images/logo-pokemon.png'
const loading = "./images/flaming_charizard.gif"


let showloading = "visible"

class App extends Component {

    render() {
        return (
            <Fragment>
                <div className="container-fluid p-0 m-0 w-100">
                    <Header logoPokemon={logo}/>
                    <Main loading={loading}/>
                    <Footer/>
                </div>
            </Fragment>
        )
    }
}

export default App;


