import React, { Component } from 'react';
import Tilty from 'react-tilty'


class Pokemon extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { img, name, types, specie, moves, id } = this.props.properties;

        return (
            <Tilty style={{ transformStyle: 'preserve-3d' }}>
                <div className={'card-pokemon shadow rounded-sm m-2 p-2 type-' + types[0] + ' type-' + types[1]} >
                    <div className="grid-pk">
                        <div className="card--header">
                            <img src={img} alt="" />
                            <span className="id-pokemon">{ id }</span>
                        </div>
                        <div className="card--body rounded p-0">
                            <div className="name pl-3">
                                <span>{name}</span>
                            </div>
                            <div className="types-pokemon  d-flex justify-content-start align-items-start p-2">
                                {
                                    types.map((type, i) => <span key={i} className="type">{type}</span>)
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </Tilty>
        )
    }
}

export { Pokemon }




