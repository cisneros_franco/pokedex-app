import React, { Component } from 'react'



class FilterFrom extends Component {

    render() {
        return (
            <div className="container-form border-0 outline-0 p-0">
                <form onSubmit={this.props.onSubmit}  className="form-filter mb-5 ml-2 mt-2">
                    <input type="text" placeholder="search your pokemon" className="p-2 border rounded" />
                    <button className="btn btn-search" type="submit"><i className="fas fa-search"></i></button>
                </form>
            </div>
        )
    }
}


export default FilterFrom;











