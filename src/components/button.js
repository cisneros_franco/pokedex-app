import React, { Component } from 'react'

export default function Button(props) {
    return (
        <button className="className=bg-light text-muted py-2 px-3 border mr-2" 
            style={{outline : 'none', border: 'none', borderRadius: '5px'}} onClick={props.onclick}>
            {props.children}
        </button>
    )
}