import React from 'react'
import ReactDOM from 'react-dom'

import 'bootstrap/dist/css/bootstrap.css'
import './estilos/css/pokemon.css'
import './estilos/css/app.css'
import './estilos/css/footer.css'
import './estilos/css/filterForm.css'
import App from './App'

const container = document.querySelector('#root');



ReactDOM.render(<App/>, container)







