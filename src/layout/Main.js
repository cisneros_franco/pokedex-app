import React, { Component } from 'react'

import ListPokemon from '../components/ListPokemon'
import FilterForm from '../components/FilterForm'
import Button from '../components/button'


class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showloading: '',
            totalList : [],
            pokeList: [],
            searchBy: 'pokemon',
            sub: false,
            startIndex: 1,
            endIndex: 60,
            nrosumador : 60
        }

        this.getPokemon = this.getPokemon.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handlePrevieous = this.handlePrevieous.bind(this);
        this.updatePokelis = this.updatePokelist.bind(this);
        this.updateLoading = this.updateLoading.bind(this);
    }

    async componentDidMount() {
        await this.getPokemon(this.state.startIndex, this.state.endIndex);   
    }

    
    async componentDidUpdate(prevProps, prevState) {
        if(this.state.startIndex !== prevState.startIndex) {
            await this.updatePokelis();
        }  
    }

    updatePokelist() {
        this.setState({
            pokeList : this.state.totalList.filter(pokemon => 
                (pokemon['id']>= this.state.startIndex && pokemon['id']<= this.state.endIndex) ? true : false
            ),
            showloading : 'visible'
        })
        this.updateLoading();
    }

    updateLoading() {
        setTimeout(() => {
            this.setState({
                showloading : 'd-none'
            })
        }, 6000)
    }

    handleSubmit(event) {
        event.preventDefault();
        const sub = event.target[0].value.trim();
        let container = [];
        if(sub.length > 0) {
            this.setState({
                pokeList :  this.state.totalList.filter(pokemon => 
                    pokemon['name'].includes(sub.toLocaleLowerCase()) ? true : false
                )})
        } else {
            this.updatePokelis();
        }  
    }

    async handleNext(event) {
        if((898 - this.state.endIndex) < this.state.nrosumador){
            await this.setState({
                startIndex : this.state.endIndex + 1,
                endIndex : 898
            })
        }
        else {
            await this.setState({
                startIndex : this.state.startIndex + this.state.nrosumador,
                endIndex : this.state.endIndex + this.state.nrosumador
            })
        }
     
    }

    async handlePrevieous() {
        if(this.state.startIndex === 0){
            return;
        }
        if(this.state.startIndex > this.state.nrosumador){
            await this.setState({
                startIndex : this.state.startIndex - this.state.nrosumador,
                endIndex : this.state.startIndex - 1
             })
        }
        else {
            await this.setState({
                startIndex : 1,
                endIndex : this.state.nrosumador
            })
        }
        
    }

    async getPokemon(start, end) {
        this.setState({
            totalList: [],
            pokeList: [],
            showloading: ''
        })

        let list = [];
        for (let index = start; index <= 898; index++) {
            if(index !== 110) {
                const data = await (await fetch(`https://pokeapi.co/api/v2/pokemon/${index}`)).json();
                this.setState({
                    totalList : [...this.state.totalList,{
                        img: data.sprites.other['official-artwork'].front_default,
                        name: data.name,
                        types: data.types.map(item => item.type.name),
                        specie: data.species.name,
                        moves: data.moves.map(moves => moves.move.name),
                        id : data.id
                    }]
                })
                if(index === this.state.nrosumador) {
                    await this.setState({
                        pokeList : [...this.state.totalList]
                    })
                    this.updateLoading();
                }
            }
        }

        /*
        await this.setState({
            totalList : list    
        }) */
    }


    

    render() {
        return (
            <div className="container p-0 m-0">
                <img src={this.props.loading} id="loading" className={this.state.showloading}/>
                <div className="search-form">
                    <FilterForm onSubmit={this.handleSubmit}/>
                </div>
                <ListPokemon pokeList={this.state.pokeList}/>
                <div className="w-100 mt-5 text-center">
                    {
                        this.state.startIndex !== 1 &&
                        <Button onclick={this.handlePrevieous}>
                            <strong>{"<"}</strong> {this.state.startIndex}
                        </Button>
                    }
                    <strong className="text-white mx-3">{this.state.endIndex}</strong>
                    {
                        this.state.endIndex < 898 && 
                        <Button onclick={this.handleNext}><span>+ {898-this.state.endIndex}</span></Button>
                    }
                </div>
            </div>
        )
    }
}


export default Main;









