import React, { Component } from 'react';

import { Pokemon } from './Pokemon'


class ListPokemon extends Component {
   
    render() {
        return (
            <div className="container-pokemon d-flex flex-wrap justify-content-center m-0 p-0">
                {
                    this.props.pokeList.map((pokemon, i) => <Pokemon key={i} properties={pokemon} />)
                }
            </div>
        )
    }
}


export default ListPokemon;









