import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return(
            <div className="footer w-100 m-0 border bg-light d-flex flex-column">
                <h3 className="text-muted mb-4">POKEDEX - V2, 3D</h3>
                <div className="creditos">
                    <span className="text-muted card-footer border">
                        Esta es la segunda version de la Pokedex, consumiendo datos desde la Poke-Api.
                        <br/>para el desarrollo se uso REACT.                        
                    </span>
                    <span className="text-muted card-footer border">
                        Los Pokemones por defecto estan enumerados desde 1 a 898.
                        para ver que los pokemones en 3D es combeniente usar una PC.
                    </span>
                    <span className="text-muted card-footer border">
                        Desarrollado por francocisneros147@gmail.com
                        <br/><strong>01/2021</strong>
                    </span>
                </div>
            </div>
        )
    }
}

export default Footer;